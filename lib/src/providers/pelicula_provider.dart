import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;

import '../models/pelicula_model.dart';
import '../models/actor_model.dart';

class PeliculasProvider {
  static const String _apiKey = '29d5ac232cbc0b3d8779a4bed8a1bff5';
  static const String _url = 'api.themoviedb.org';
  static const String _version = '3';
  static const String _language = 'es-CO';

  static int _popularesPage = 0;
  static bool _cargando = false;

  static List<Pelicula> _populares = new List<Pelicula>();

  static final _popularesStreamController =
      new StreamController<List<Pelicula>>.broadcast();

  static void disposeStreams() {
    _popularesStreamController?.close();
  }

  static Function(List<Pelicula>) get popularesSink =>
      _popularesStreamController.sink.add;

  static Stream<List<Pelicula>> get popularesStream =>
      _popularesStreamController.stream;

  static Future<List<Pelicula>> _processGet(
    String action, [
    int page = 1,
  ]) async {
    final String actionVersion = '$_version/$action';

    final url = Uri.https(_url, actionVersion, {
      'api_key': _apiKey,
      'language': _language,
      'page': page.toString(),
    });

    final resp = await http.get(url);
    final decodedData = json.decode(resp.body);

    final peliculas = new Peliculas.fromJSONList(decodedData['results']);

    return peliculas.items;
  }

  static Future<List<Pelicula>> getEnCines() async {
    return await _processGet('movie/now_playing');
  }

  static Future<List<Pelicula>> getPopulares() async {
    if (_cargando) return [];
    _cargando = true;

    ++_popularesPage;

    // print('Cargando siguientes ...');

    final resp = await _processGet('movie/popular', _popularesPage);
    _populares.addAll(resp);
    popularesSink(_populares);

    _cargando = false;

    return resp;
  }

  static Future<List<Actor>> getCast(int peliculaId) async {
    final url = Uri.https(_url, '$_version/movie/$peliculaId/credits', {
      'api_key': _apiKey,
      'language': _language,
    });
    final resp = await http.get(url);
    final decodedData = json.decode(resp.body);
    final actores = new Actores.fromJSONList(decodedData['cast']);
    return actores.items;
  }

  static Future<List<Pelicula>> buscarPelicula(String query) async {
    final url = Uri.https(_url, '$_version/search/movie', {
      'api_key': _apiKey,
      'language': _language,
      'query': query,
    });
    final resp = await http.get(url);
    final decodedData = json.decode(resp.body);

    final peliculas = new Peliculas.fromJSONList(decodedData['results']);

    return peliculas.items;
  }
}
