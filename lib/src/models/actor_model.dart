class Actores {
  List<Actor> items = new List<Actor>();

  Actores();

  Actores.fromJSONList(List<dynamic> jsonList) {
    if (jsonList == null) return;
    jsonList.forEach((var i) {
      final actor = new Actor.fromJSONMap(i);
      items.add(actor);
    });
  }
}

class Actor {
  int castId;
  String character;
  String creditId;
  int gender;
  int id;
  String name;
  int order;
  String profilePath;

  Actor({
    this.castId,
    this.character,
    this.creditId,
    this.gender,
    this.id,
    this.name,
    this.order,
    this.profilePath,
  });

  factory Actor.fromJSONMap(Map<String, dynamic> json) => Actor(
        castId: json["cast_id"],
        character: json["character"],
        creditId: json["credit_id"],
        gender: json["gender"],
        id: json["id"],
        name: json["name"],
        order: json["order"],
        profilePath: json["profile_path"] == null ? null : json["profile_path"],
      );

  getFoto() {
    if (profilePath == null)
      return 'https://tutela.ca/img/noavatar.png';
    return 'https://image.tmdb.org/t/p/w500/$profilePath';
  }
}
