class Peliculas {
  List<Pelicula> items = new List<Pelicula>();

  Peliculas();

  Peliculas.fromJSONList(List<dynamic> jsonList) {
    if (jsonList == null) return;
    jsonList.forEach((var i) {
      final pelicula = new Pelicula.fromJSONMap(i);
      items.add(pelicula);
    });
  }
}

class Pelicula {
  String uniqueId;
  double popularity;
  int voteCount;
  bool video;
  String posterPath;
  int id;
  bool adult;
  String backdropPath;
  String originalLanguage;
  String originalTitle;
  List<int> genreIds;
  String title;
  double voteAverage;
  String overview;
  DateTime releaseDate;

  Pelicula({
    this.popularity,
    this.voteCount,
    this.video,
    this.posterPath,
    this.id,
    this.adult,
    this.backdropPath,
    this.originalLanguage,
    this.originalTitle,
    this.genreIds,
    this.title,
    this.voteAverage,
    this.overview,
    this.releaseDate,
  });

  factory Pelicula.fromJSONMap(Map<String, dynamic> json) => Pelicula(
        popularity: json["popularity"].toDouble(),
        voteCount: json["vote_count"],
        video: json["video"],
        posterPath: json["poster_path"],
        id: json["id"],
        adult: json["adult"],
        backdropPath:
            json["backdrop_path"] == null ? null : json["backdrop_path"],
        originalLanguage: json["original_language"],
        originalTitle: json["original_title"],
        genreIds: List<int>.from(json["genre_ids"].map((x) => x)),
        title: json["title"],
        voteAverage: json["vote_average"].toDouble(),
        overview: json["overview"],
        releaseDate: DateTime.parse(json["release_date"]),
      );

  getPosterImg() {
    if (posterPath == null)
      return 'https://s.movieinsider.com/images/none_175px.jpg';
    return 'https://image.tmdb.org/t/p/w500/$posterPath';
  }

  getBackdropImg() {
    if (backdropPath == null)
      return 'https://s.movieinsider.com/images/none_175px.jpg';
    return 'https://image.tmdb.org/t/p/w500/$backdropPath';
  }
}
